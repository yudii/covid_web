# covid_web



<p align="center">
    <img width="" src="https://docsify.js.org/_media/icon.svg" >
</p>



[![star](https://gitee.com/yudii/covid_web/badge/star.svg?theme=dark)](https://gitee.com/veal98/Echo/stargazers)
[![fork](https://gitee.com/yudii/covid_web/badge/fork.svg?theme=dark)](https://gitee.com/veal98/Echo/members)
[![version](https://img.shields.io/badge/version-1.0-brightgreen)]()
<a href="#-微信交流群"><img src="https://img.shields.io/badge/交流-微信群-orange" alt="交流群"></a>



> 云服务器9.26到期，不准备续费，未上手的小伙伴可以看下面的界面展示了解本项目，所有代码在我本机上都是正常运行的，并欢迎加群讨论技术问题。
>
> 演示地址：http://8.130.170.99/myapp/index

## 📚 项目简介

covid_web 是一套前后端不分离数据统计系统，基于目前主流 Python Web 技术栈（python + csv + django+  request）去实现的爬虫小项目，并提供详细的开发文档和配套教程。

**源码链接**：已托管在Gitee：

- Gitee：[🆑](https://gitee.com/yudii/covid_web)

**文档地址**：文档通过 <u>Docsify + Gitee Pages</u> 生成

- Gitee Pages：[🆑](https://yudii.gitee.io/covid_web/#/)


## 💻 核心技术栈

后端：

- Python
- Django

前端：

- Bootstrap 3.x
- Echart
- Jquery
- Ajax

## 🔨 开发环境

- 操作系统：Windows 10
- 构建工具：Apache Maven
- 集成开发工具：Intellij IDEA
- 应用服务器：测式使用Django自带，上线使用uWSGI+Nginx
- 接口测试工具：Postman
- 版本控制工具：Gitee
- python 版本：3.8.8

## 🎀 界面展示

首页：

![](https://gitee.com/yudii/private/raw/master/covid_web/home%20(1).png)

数据页：

![](https://gitee.com/yudii/private/raw/master/covid_web/data.png)

介绍页面：

![](https://gitee.com/yudii/private/raw/master/covid_web/about.png)

docsify主题

![](https://gitee.com/yudii/private/raw/master/covid_web/wiki.png)

docsify文档主要内容

![](https://gitee.com/yudii/private/raw/master/covid_web/wiki_pic.png)

## 🔐 待实现及优化

以下是我觉得本项目还可以添加的功能，同样欢迎各位小伙伴提 issue 指出还可以增加哪些功能，或者直接提 PR 实现该功能：

- [ ] 数据分析，需要获取到更加直观可靠的数据

- [ ] 提供最新疫情新闻栏目

- [ ] 省的具体市的疫情数据

- [ ] 支持邮件订阅疫情信息

  ...

## 🌱 本地运行

各位如果需要将项目部署在本地进行运行，以下环境请提前备好：

- Python3.8.8

- Django2.3.7

  





## 🌌 部署架构

我每个都只部署了一台，以下是理想的部署架构：

<img src="https://gitee.com/yudii/private/raw/master/covid_web/t.png"  />

> 按照上面的部署图，我们基于centos7系统下，使用Nginx作为反向代理，uWSGI作为动态资源响应，进行部署。如果您想学习centos+django+uwsgi的部署技术再次不过多讲解，这里主要讲一些坑。

**配置可以参考官网 [🆑](https://www.django.cn/article/show-4.html#banqian)**

### 1.win安装uwsgi



> 结论就是有困难



尝试过pip3下载，然后官网下载使用setup安装，报os是linux操作系统的，换成platform对吧然后导入platform包对吧。还有就是没有GCC的环境对吧，解决！还是报错，最后放弃了。

还是在linux操作系统上装，不影响，起初想在本地安装一个是想感受一下这uwsgi的用法，你也可以在linux运行一个入门程序。

### 2.nginx安装的坑

当你安装好nginx，准备来一波脚本操作

```bash
./configration
make
make install
```

好了一大堆错来了,首先把包下好，这个nginx依赖的包

```bash
1.gcc的有
yum install gcc 
yum install gcc-c++

2.nginx依赖的包
yum -y install pcre*
yum -y install zlib*
yum -y install openssl 
yum -y install openssl-devel
```

后面还有一下错误什么没有这个function，c里wrong被视为错误，找到这个function注释掉，找到这个wrong删掉，然后再run一遍，ok

### 3.静态资源没有加载到

参考官网示例

1.检查uwsgi.ini的配置

```bash
#添加配置选择
[uwsgi]
#配置和nginx连接的socket连接
socket=127.0.0.1:8997
#配置项目路径，项目的所在目录
chdir=/data/wwwroot/mysite/
#配置wsgi接口模块文件路径,也就是wsgi.py这个文件所在的目录名
wsgi-file=mysite/wsgi.py
#配置启动的进程数
processes=4
#配置每个进程的线程数
threads=2
#配置启动管理主进程
master=True
#配置存放主进程的进程号文件
pidfile=uwsgi.pid
#配置dump日志记录
daemonize=uwsgi.log`
```

2.检查nginx.cof的配置

```bash
events {
    worker_connections  1024;
}
http {
    include       mime.types;
    default_type  application/octet-stream;
    sendfile        on;
    server {
        listen 80;
        server_name  www.django.cn; #改为自己的域名，没域名修改为127.0.0.1:80
        charset utf-8;
        location / {
           include uwsgi_params;
           uwsgi_pass 127.0.0.1:8997;  #端口要和uwsgi里配置的一样
           uwsgi_param UWSGI_SCRIPT mysite.wsgi;  #wsgi.py所在的目录名+.wsgi
           uwsgi_param UWSGI_CHDIR /data/wwwroot/mysite/; #项目路径
           
        }
        location /static/ {
        alias /data/wwwroot/mysite/static/; #静态资源路径
        }
    }
}
```

3.自己项目里面的settiing.py

```bash
STATIC_ROOT = '/wwwroot/mysite/static'  #指定样式收集目录
```

### 4.python导包不需要venv

如果再pycharm有venv，上线的时候删掉，我们最好安装一个pyweb虚拟环境，再虚拟环境中用uwsgi运行我们的py项目

### 5.docsify技术



> 文档通过 Docsify + Github/Gitee Pages 生成



去gitee上进行认证，然后生成文档运行域名，这个算是个惊喜吧，使用docsify确实太美观了，具体配置网上太多，在这link[🆑](https://docsify.js.org/#/zh-cn/quickstart)

```bash
#启动本地docsify服务，port3000
docsify serve 
```



## 📞 联系我

有什么问题也可以添加我的微信：

<img width="220px" src="https://gitee.com/yudii/private/raw/master/covid_web/my_v.jpg" >

## 🙋 微信交流群

下方扫码加入**放码过来 **微信交流群，实时跟进项目进度，分享您的想法，还能帮您解决遇到的问题。如果二维码过期可以上方扫码加我微信回复 “交流群”：

<img width="235px" src="https://gitee.com/yudii/private/raw/master/covid_web/group_v.jpg" >

## 👏 鸣谢

水平有限,很多只是为了去了解一些技术然后东拼西凑一个这样的项目，本项目大部参考开源项目，在此感谢。

另外，若发现 Bug 或好的想法可以积极与我联系或提 PR / issue，采纳后您将出现在下方列表中。感谢以下小伙伴对本项目做出的贡献，排名按照时间先后：

> 🔗 友情链接（若您想要出现在这里，可以上方扫描微信二维码联系我）：
>
> - [CS-Wiki](https://gitee.com/veal98/CS-Wiki)：致力打造完善的 Java 后端知识体系，不仅仅帮助各位小伙伴快速且系统的准备面试，更指引学习的方向
> - [Furion](https://gitee.com/dotnetchina/Furion)：让 .NET 开发更简单，更通用，更流行
> - [Free-Fs](https://gitee.com/dh_free/free-fs)：Spring Boot 开源云文件管理系统，方便快捷的管理云存储的文件

> 这是一个基于python进行开发的爬虫项目，是对疫情数据的统计汇总
