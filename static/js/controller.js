function gettime() {
	$.ajax({
		url: '/myapp/time',
		type: "get",
		success: function(data) {
			$('#time').html(data)
		},
		error: function() {

		}

	});
}

function get_c1_data() {
	$.ajax({
		url: '/myapp/center1',
		type: "get",
		success: function(data) {
			$('.data_bg h1').eq(0).text(data.today_confirm);
			$('.data_bg h1').eq(1).text(data.now_confirm);
			$('.data_bg h1').eq(2).text(data.heal);
			$('.data_bg h1').eq(3).text(data.dead);
		},
		error: function() {
		}
	});
}

function get_c2_data() {
    $.ajax({
        url: "/myapp/center2",
        success: function (data) {
            ec_center.hideLoading();
            ec_center_option.series[0].data=data.data
            ec_center.setOption(ec_center_option);
        }
    })
}
function get_l1_data() {
    $.ajax({
        url: "/myapp/left1",
        success: function (data) {
	    ec_left1.hideLoading();
            ec_left1_Option.xAxis[0].data=data.date;
            ec_left1_Option.series[0].data=data.confirm;
            ec_left1_Option.series[1].data=data.suspect;
            ec_left1_Option.series[2].data=data.heal;
            ec_left1_Option.series[3].data=data.dead;
            ec_left1.setOption(ec_left1_Option);
        }
    })
}

function get_l2_data() {
    $.ajax({
        url: "/myapp/left2",
        success: function (data) {
	    ec_left2.hideLoading();
            ec_left2_Option.xAxis[0].data=data.date;
            ec_left2_Option.series[0].data=data.confirm;
            ec_left2_Option.series[1].data=data.suspect;
            ec_left2.setOption(ec_left2_Option);
        }
    })
}

function get_r1_data() {
    $.ajax({
        url: "/myapp/right1",
        success: function (data) {
	    ec_right1.hideLoading();
            ec_right1_option.xAxis.data=data.city;
            ec_right1_option.series[0].data=data.confirm;
            ec_right1.setOption(ec_right1_option);
        }
    })
}

function get_r2_data() {
    $.ajax({
        url: "/myapp/right2",
        success: function (data) {
	    ec_right2.hideLoading();
            ec_right2_option.series[0].data=data.kws;
            ec_right2.setOption(ec_right2_option);
        }
    })
}


gettime();
get_c1_data();
get_c2_data();
get_l1_data();
get_l2_data();
get_r1_data();
get_r2_data();

setInterval(gettime, 1000);
setInterval(get_c1_data, 1000*3600); // 60s更新一次
setInterval(get_c2_data, 1000*3600);
setInterval(get_l1_data, 1000*3600);
setInterval(get_l2_data, 1000*3600);
setInterval(get_r1_data, 1000*3600);
setInterval(get_r2_data, 1000*3600); // 120s更新一次 词云图