var ec_center = echarts.init(document.getElementById('center2'), "dark");

var ec_center_option = {
    bgColor: '#354e90',
    backgroundColor: 'rgba(128, 128, 128, 0.0)',
    title: {
        text: '疫情今日确诊人数',
        subtext: '',
        x: 'left'
    },
    tooltip: {
        trigger: 'item'
    },
    //左侧小导航图标
    visualMap: {
        show: true,
        x: 'left',
        y: 'bottom',
        textStyle: {
            fontSize: 8,
        },
        splitList: [{
            start: 1,
            end: 9
        },
            {
                start: 10,
                end: 99
            },
            {
                start: 100,
                end: 999
            },
            {
                start: 1000,
                end: 9999
            },
            {
                start: 10000
            },
        ],
        color: ['#8A3310', '#C64918', '#E55B25', '#F2AD92', '#F9DCD1']
    },
    //配置属性
    series: [{
        name: '累计确诊人数',
        type: 'map',
        mapType: 'china',
        roam: true, //拖动和缩放
        itemStyle: {
            normal: {
                borderWidth: .5, //区域边框宽度
                borderColor: '#009fe8', //区域边框颜色
                areaColor: "rgba(128, 128, 128, 0.1)", //区域颜色
            },
            emphasis: { //鼠标滑过地图高亮的相关设置
                borderWidth: 1,
                borderColor: '#009fe8',
                areaColor: "rgba(20, 18, 100, 0.5)",
            }
        },
        label: {
            normal: {
                show: true, //省份名称
                fontSize: 10,
                color: '#FFFFFF'
            },
            emphasis: {
                show: true,
                fontSize: 10,
                color: '#FFFFFF'
            }
        },
        data: [] //数据
    }]
};
ec_center.setOption(ec_center_option)
