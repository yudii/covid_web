import datetime
import pandas
import myapp.dao.getData
import myapp.dao.getChinaDayAddList


def time_json():
    t = datetime.datetime.now().strftime("%Y年%m月%d日 %H:%M:%S")
    return t


def center1_json():
    # myapp.dao.getData.get_data()
    fp = pandas.read_csv('static/csv/today.csv', encoding='utf-8')
    # 今日确诊
    today_confirm = 0
    # 现存确诊
    now_confirm = 0;
    # 总治愈人数
    heal = 0
    # 总死亡人数
    dead = 0
    for index, row in fp.iterrows():
        today_confirm += row['today_confirm']
        now_confirm += row['now_confirm']
        heal += row['heal']
        dead += row['dead']
    t = {}
    t['today_confirm'] = today_confirm
    t['now_confirm'] = now_confirm
    t['heal'] = heal
    t['dead'] = dead
    return t


def center2_json():
    # myapp.dao.getData.get_data()
    fp = pandas.read_csv('static/csv/today.csv', encoding='utf-8')
    t = {'data': []}
    list = []
    for index, row in fp.iterrows():
        list.append({'name': row['province'], 'value': row['today_confirm']})
    t['data'] = list
    return t


def left1_json():
    # myapp.dao.getData.get_data()
    t = {
        'date': [],
        'confirm': [],
        'suspect': [],
        'dead': [],
        'heal': []
    }
    fp = pandas.read_csv('static/csv/month.csv', encoding='utf-8')
    for index, row in fp.iterrows():
        t['date'].append(row['date'])
        t['confirm'].append(row['confirm'])
        t['suspect'].append(row['suspect'])
        t['dead'].append(row['dead'])
        t['heal'].append(row['heal'])
    return t


def left2_json():
    # myapp.dao.getData.get_data()
    fp = pandas.read_csv('static/csv/month.csv')
    t = {
        'date': [],
        'confirm': [],
        'suspect': []
    }
    for index, row in fp.iterrows():
        t['date'].append(row['date'])
        t['confirm'].append(row['confirm'])
        t['suspect'].append(row['suspect'])
    return t


def right1_json():
    # myapp.dao.getData.get_data()
    fp = pandas.read_csv('static/csv/today.csv')
    t = {
        'city': [],
        'confirm': []
    }
    for index, row in fp.iterrows():
        if row['today_confirm'] > 0:
            t['city'].append(row['province'])
            t['confirm'].append(row['confirm'])
    return t


def right2_json():
    # myapp.dao.getData.get_data()
    fp = pandas.read_csv('static/csv/today.csv')
    t = {'kws': []}
    for index, row in fp.iterrows():
        if row['today_confirm'] > 0:
            t['kws'].append({'name': row['province'], 'value': row['today_confirm']})
    return t
