from django.urls import path

import myapp.views

urlpatterns = [
    path('index', myapp.views.index),
    path('data', myapp.views.data),
    path('about', myapp.views.about),

    path('time', myapp.views.time),

    path('center1', myapp.views.center1),
    path('center2', myapp.views.center2),

    path('left1', myapp.views.left1),
    path('left2', myapp.views.left2),

    path('right1', myapp.views.right1),
    path('right2', myapp.views.right2)
]