from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from myapp.service import getData


# Create your views here.
def index(request):
    return render(request, "index.html")


def data(request):
    return render(request, 'data.html')


def about(request):
    return render(request, 'about.html')


def time(request):
    json = getData.time_json()
    return HttpResponse(json)


def center1(request):
    json = getData.center1_json()
    return JsonResponse(json)


def center2(request):
    json = getData.center2_json()
    return JsonResponse(json)


def left1(request):
    json = getData.left1_json()
    return JsonResponse(json)


def left2(request):
    json = getData.left2_json()
    return JsonResponse(json)


def right1(request):
    json = getData.right1_json()
    return JsonResponse(json)


def right2(request):
    json = getData.right2_json()
    return JsonResponse(json)
