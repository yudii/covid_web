<!-- _coverpage.md -->
![logo](https://docsify.js.org/_media/icon.svg)
# covid-19 <small>0.0.1</small>

> 一个基于python的爬虫项目。
- 后端框架：python+Django
- 前端框架：Bootstrap+Echart
- 部署技术：Nginx+uWSGI

<!-- 按钮部分 -->
[Gitee](https://gitee.com/yudii/covid_web/)
[快速开始](#docsify)
